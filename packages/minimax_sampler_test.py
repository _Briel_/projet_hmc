import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams['agg.path.chunksize'] = 10000
from truncated_mvn_sampler.minimax_tilting_sampler import TruncatedMVN

d = 2  # dimensions

# random mu and cov
mu = np.array([4, 4])
cov = 0.5 - np.random.rand(d ** 2).reshape((d, d))
cov = np.triu(cov)
cov += cov.T - np.diag(cov.diagonal())
cov = np.identity(d)

# constraints
lb = np.zeros_like(mu) + 2
ub = np.ones_like(mu) + 6
print(lb)
print(ub)

# create truncated normal and sample from it
n_samples = 100000
tmvn = TruncatedMVN(mu, cov, lb, ub)
samples = tmvn.sample(n_samples)
#attention taille d*n (horizontale)


plt.plot(samples[0], samples[1], marker='p')
plt.show()
