import numpy as np
import matplotlib.pyplot as plt
from HMC_TruncatedGaussian.python.hmc_tmg import HMCTruncGaussian


size = 2
mean = [0] * size
cov_mtx = np.identity(size)
#Fc = -np.array([[1,0], [0,1],[-1, 1], [1.1, -1]])
#g = np.zeros((4,1))
Fc=np.array([ [-1,0],[0,-1]])
g=np.array([ [0.7], [0.2]])
initial = np.array([[0.01], [0.01]])
sample=HMCTruncGaussian().generate_general_tmg(Fc, g, cov_mtx, mean, initial, 20)

print(sample)

plt.xlim(0, 1)
plt.ylim(0, 1)
x1=[]
x2=[]
for pt in sample:
    x1.append(pt[0])
    x2.append(pt[1])
plt.plot(x1, x2, marker='p')
xmin = -10
xmax = 12
#vecteur x naifs
x = np.linspace(xmin, xmax, 100)

d=len(g)
for i in range(d):
    a,b=Fc[i][0],Fc[i][1]
    if a==0:
        plt.axvline(g[i], color='blue', lw=2)
    elif b==0:
        plt.axhline(g[i], color='green', lw=2)
    else :
        plt.plot(x, (g[i]-a*x)/b, color='red')
plt.title('HMC from package HMC_TruncatedGaussian')  #
plt.show()